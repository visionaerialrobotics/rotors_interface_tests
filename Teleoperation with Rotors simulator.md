##  Teleoperation with Rotors simulator

This test verifies the correct teleoperation with Rotors simulator.

In order to execute this test, perform the following steps:

- Install the Aerostack project called "teleoperation_rotors_simulator".

- Change directory to this project:

        $ cd teleoperation_rotors_simulation

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

- The following window for teleoperation is presented:

![capture teleoperation.png](https://bitbucket.org/repo/rokr9B/images/3392326917-capture%20teleoperation.png)

- Teleoperate the drone with the alphanumeric interface using the appropriate keys.

Here there is a video that shows the correct execution of the test:

[ ![Python Mission](https://img.youtube.com/vi/TOC_p9kh1Tc/0.jpg)](https://youtu.be/TOC_p9kh1Tc)

