##  Basic mission with Rotors simulator

This test verifies the correct execution of a mission (written in Python) with Rotors simulator.

In order to execute this test, perform the following steps:

- Install the Aerostack project called "basic_mission_rotors_simulation".

- Change directory to this project:

        $ cd basic_mission_rotors_simulation

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

- In order to run the mission you have to execute the following command:

        $ rosservice call /drone111/python_based_mission_interpreter_process/start

Here there is a video that shows the correct execution of the test:

[ ![Python Mission](https://img.youtube.com/vi/cNk78acSQzo/0.jpg)](https://youtu.be/cNk78acSQzo)